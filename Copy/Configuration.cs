using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.Linq;

namespace Copy
{
    public partial class Configuration : Form
    {
        Dictionary<string, CheckBox> checkBoxes;
        char[] caps = Enumerable.Range('A', 'Z' - 'A' + 1).Select(c => (Char)c).ToArray();

        public Configuration()
        {
            InitializeComponent();
            checkBoxes = new Dictionary<string, CheckBox>();
        }

        private void LoadSettings(object sender, EventArgs e)
        {
            Properties.Settings.Default.Reload();

            int i = 0;

            foreach (SettingsProperty currentProperty in Properties.Settings.Default.Properties)
            {
                if (Properties.Settings.Default[currentProperty.Name].GetType() == typeof(bool))
                {
                    string name = currentProperty.Name.Substring(0, 1);
                    int n = 1;
                    bool loop = true;

                    while (loop)
                    {
                        int oldN = n;
                        n = currentProperty.Name.IndexOfAny(caps, oldN + 1);

                        if (n == -1) //if nothing was found
                        {
                            n = currentProperty.Name.Length;
                            loop = false;
                        }

                        if (oldN == 1)
                        {
                            name += currentProperty.Name.Substring(oldN, n - oldN);
                        }
                        else
                        {
                            name += " " + currentProperty.Name.Substring(oldN, n - oldN).ToLower();
                        }
                    }

                    checkBoxes[currentProperty.Name] = new CheckBox()
                    {
                        Checked = (bool)Properties.Settings.Default[currentProperty.Name],
                        Name = currentProperty.Name + "Check",
                        Text = name,
                        Location = new Point(5, i * 22)
                    };
                    Controls.Add(checkBoxes[currentProperty.Name]);
                    Action<object, EventArgs> checkedChanged = (object s, EventArgs ev) => { Properties.Settings.Default[currentProperty.Name] = checkBoxes[currentProperty.Name].Checked; };
                    checkBoxes[currentProperty.Name].CheckedChanged += new EventHandler(checkedChanged);
                    i++;
                }
                else
                {
                    //non-bool settings go here
                }
            }

            Height = i * 22 + 40;
        }

        private void SaveSettings(object sender, FormClosingEventArgs e)
        {
            Properties.Settings.Default.Save();
        }
    }
}