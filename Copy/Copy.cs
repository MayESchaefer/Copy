using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Linq;
using WK.Libraries.SharpClipboardNS;

namespace Copy
{
    public class SysTrayApp : ApplicationContext
    {
        NotifyIcon notifyIcon = new NotifyIcon();
        Configuration configWindow = new Configuration();
        SharpClipboard clipboard = new SharpClipboard();
        Hotkeys hotkeys = new Hotkeys();
        
        public SysTrayApp()
        {
            MenuItem configMenuItem = new MenuItem("Options", new EventHandler(ShowConfig));//maybe make the options panel a sub-MenuItem? Idfk.
            MenuItem exitMenuItem = new MenuItem("Exit", new EventHandler(Exit));

            clipboard.MonitorClipboard = true;
            clipboard.ObservableFormats.All = false;
            clipboard.ObservableFormats.Texts = true;
            clipboard.ClipboardChanged += ClipboardChanged;

            notifyIcon.Icon = Properties.Resources.AppIcon;
            notifyIcon.ContextMenu = new ContextMenu(new MenuItem[] { configMenuItem, exitMenuItem });
            notifyIcon.Click += ShowConfig;
            notifyIcon.Visible = true;

            Hotkey clearClipboard = new Hotkey(new Action(ClearClipboard), 3, Keys.C);

            hotkeys.SetHotkey(clearClipboard);
        }

        void ClipboardChanged(object sender, SharpClipboard.ClipboardChangedEventArgs e)
        {
            if (e.ContentType == SharpClipboard.ContentTypes.Text)
            {
                clipboard.MonitorClipboard = false;
                if (Properties.Settings.Default.SpacesToTabs)
                {
                    Clipboard.SetText(SpacesToTabs(clipboard.ClipboardText));
                }
                clipboard.MonitorClipboard = true;
            }
        }

        string SpacesToTabs (string text)
        {
            HashSet<int> spaceLens = new HashSet<int>();

            foreach (string line in text.Split('\n'))
            {
                int spaces = line.TakeWhile(c => c == ' ').Count();
                if (spaces > 0)
                {
                    spaceLens.Add(spaces);
                }
            }

            List<int> numSpaces = spaceLens.OrderBy(x => x).ToList();
            numSpaces.Remove(1);

            bool looksLikeTabs = true;
            if (numSpaces.Count > 0)
            {
                for (int i = 1; i < numSpaces.Count; i++)
                {
                    if (numSpaces[i] % numSpaces[0] != 0)
                    {
                        looksLikeTabs = false;
                        break;
                    }
                }

                if (looksLikeTabs)
                {
                    return text.Replace("".PadLeft(numSpaces[0], ' '), "\t");
                }
            }
            return text;
        }

        void ClearClipboard ()
        {
            if (Properties.Settings.Default.ClearClipboardHotkey)
            {
                Clipboard.Clear();
            }
        }

        void ShowConfig(object sender, EventArgs e)
        {
            if (configWindow.Visible)
            {
                configWindow.Focus();
            }
            else
            {
                configWindow.ShowDialog();
            }
        }

        void Exit(object sender, EventArgs e)
        {
            notifyIcon.Visible = false;
            hotkeys.UnsetHotkeys();
            Application.Exit();
        }
    }
}
